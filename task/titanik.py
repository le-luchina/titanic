import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    counts = []
    for title in (' Mr. ', ' Mrs. ', ' Miss. '):
        mr = df[df['Name'].str.contains(title)]
        mr_mean = int(mr['Age'].mean())
        mr_na = mr['Age'].isna().sum()
        counts.append((title, mr_na, mr_mean))

    return counts
